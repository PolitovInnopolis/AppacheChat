package inno.politov;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

import javax.jms.*;

/**
 * Created by General on 3/14/2017.
 */
public class Producer implements Runnable {
//    private static Logger logger = Logger.getLogger(Producer.class);

    public void run() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                "vm://localhost");
        Connection connection = null;
        try {
            connection = factory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("message queue dest");
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            TextMessage textMessage = session.createTextMessage("Hello World!");
            producer.send(textMessage);
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
