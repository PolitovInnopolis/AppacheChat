package inno.politov;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by General on 3/14/2017.
 */
public class Consumer implements Runnable {
    //    private static Logger logger = Logger.getLogger(Producer.class);

    @Override
    public void run() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                "vm://localhost");
        Connection connection = null;
        try {
            connection = factory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("message queue dest");
            MessageConsumer consumer = session.createConsumer(destination);
            Message message = consumer.receive(10000);
            System.out.println(((TextMessage)message).getText());
            session.close();
            connection.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
