package inno.politov;

/**
 * Created by General on 3/14/2017.
 */
public class Main {
    public static void main(String[] args) {
        Thread consumerTh = new Thread(new Consumer());
        consumerTh.start();
        Thread producerTh = new Thread(new Producer());
        producerTh.start();
    }
}
